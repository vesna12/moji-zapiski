# Moji zapiski s predavanj in vaj na FMF #

### Vsebina ###
* Algebra 3, predavanja in vaje
* Analiza 4, predavanja
* Fizika 1, predavanja in vaje
* Mehanika, vaje delno
* Moderna fizika (mof), predavanja in vaje delno
* Programiranje 2, predavanja
* Statistika, predavanja in vaje
* Uvod v geometrijsko topologijo (ugt), predavanja in vaje

Opomba: seveda gre zahvala sošolcem, ki so zapiske skenirali, ter predvsem predavateljem in asistentom, ki so nam snov razlagali.